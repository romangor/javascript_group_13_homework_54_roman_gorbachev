import { Component } from '@angular/core';
import { Square } from "../squares/square";

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.css']
})
export class ControlPanelComponent  {

  squares = [new Square(),new Square(),new Square(),new Square(),new Square(),new Square(),
    new Square(),new Square(),new Square(),new Square(),new Square(),new Square(),
    new Square(),new Square(),new Square(),new Square(),new Square(),new Square(),
    new Square(),{hasItem: true, show: false},new Square(),new Square(),new Square(),new Square(),
    new Square(),new Square(),new Square(),new Square(),new Square(),new Square(),
    new Square(),new Square(),new Square(),new Square(),new Square(),new Square()];

  tries = 0;

  getRandomSymbol() {
    return Math.floor(Math.random() * (37 - 1)) + 1;
  }

  onReset(){
    this.squares = [];
    this.tries = 0;
    const randomNumber = this.getRandomSymbol();
    for (let i = 0; i < 36; i++){
      this.squares.push(new Square);
      if(i === randomNumber){
        this.squares[i].hasItem = true;
      }
    }
    return this.squares;
  }

  toTry() {
    return this.tries ++;
  }

}


