import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Square} from "./square";

@Component({
  selector: 'app-squares',
  templateUrl: './squares.component.html',
  styleUrls: ['./squares.component.css']
})
export class SquaresComponent  {
  @Input() squares = [new Square()];
  @Output() tries = new EventEmitter();
  @Output() finish = new EventEmitter();

  clickSquare(event: Event, index: number) {
    this.squares[index].show = true;
    this.tries.emit(event);
     if(this.showItem(index)) {
       alert('Вы нашли предмет');
     }
  }

  changeClass(index: number) {
    const className = this.squares[index].show ? 'white' : 'black';
    return 'square-' + className;
  }

  showItem(index: number){
    return this.squares[index].hasItem && this.squares[index].show;
  }

}
